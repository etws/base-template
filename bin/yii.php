#!/usr/bin/env php
<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../app/bootstrap.php');

$application = new ETWS\Framework\Console\Application();
$application->run();

$exitCode = $application->run();
exit($exitCode);
