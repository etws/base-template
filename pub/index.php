<?php
//error_reporting(-1);
//ini_set("display_errors", 1);
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../app/bootstrap.php');

/** @var \Composer\Autoload\ClassLoader $vendorAutoload */
$application = new \ETWS\Framework\Web\Application($vendorAutoload);
$application->run();