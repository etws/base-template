<?php

$pathList[] = glob(BP . '/app/code/*/*/registration.php', GLOB_NOSORT);
$pathList[] = glob(BP . '/app/themes/*/*/*/registration.php', GLOB_NOSORT);

foreach ($pathList as $path) {
    foreach ($path as $file) {
        include $file;
    }
}