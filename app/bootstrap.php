<?php
define('BP', dirname(__DIR__));

$vendorAutoload = BP . '/vendor/autoload.php';
$yii = BP . '/vendor/yiisoft/yii2/Yii.php';

if (file_exists($vendorAutoload)) {
    $vendorAutoload = require($vendorAutoload);
}
if (file_exists($yii)) {
    require($yii);
}